FROM registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest

RUN apt-get update && apt-get install -y \
    build-essential \
    net-tools \
    libcurl4-openssl-dev \
    procps \
    python3 \
    python3-pip \
    && apt-get clean

COPY . /buildbox-e2e

ENV BUILDBOX_E2E_ROOT=/buildbox-e2e-build BUILDBOX_E2E_DATA_DIR=/buildbox-e2e/data PATH="/buildbox-e2e/bin:${PATH}"

RUN end-to-end-test.sh
