#!/bin/sh

set -e

if [ -z $BUILDBOX_E2E_ROOT ]; then
    echo 'Error: you must specify a $BUILDBOX_E2E_ROOT!'
    exit 1
fi

if [ -z $BUILDBOX_E2E_DATA_DIR ]; then
    echo 'Error: you must specify a $BUILDBOX_E2E_DATA_DIR!'
    exit 1
fi

echo 'BUILDBOX E2E TEST'
echo '================='

# Set (and print) default values for variables

echo 'buildbox-common source root:' ${BUILDBOX_COMMON_SOURCE_ROOT:=$BUILDBOX_E2E_ROOT/buildbox-common}
echo 'buildbox-worker source root:' ${BUILDBOX_WORKER_SOURCE_ROOT:=$BUILDBOX_E2E_ROOT/buildbox-worker}
echo 'buildbox-worker extra arguments:' $BUILDBOX_WORKER_EXTRA_ARGS
echo 'buildbox-run source root:' ${BUILDBOX_RUN_SOURCE_ROOT:=$BUILDBOX_E2E_ROOT/buildbox-run}
echo 'buildbox-run command name:' ${BUILDBOX_RUN:=buildbox-run-hosttools}
echo 'BuildGrid source root:' ${BUILDGRID_SOURCE_ROOT:=$BUILDBOX_E2E_ROOT/buildgrid}
echo 'RECC source root:' ${RECC_SOURCE_ROOT:=$BUILDBOX_E2E_ROOT/recc}

rm -rf $BUILDBOX_E2E_ROOT
mkdir -p $BUILDBOX_E2E_ROOT

# Download projects if needed

# TODO: remove branch options once everything's merged
if [ ! -d $BUILDBOX_COMMON_SOURCE_ROOT ]; then
    echo 'Downloading buildbox-common...'
    git clone --depth 1 https://gitlab.com/BuildGrid/buildbox/buildbox-common.git "$BUILDBOX_COMMON_SOURCE_ROOT"
fi
if [ ! -d $BUILDBOX_WORKER_SOURCE_ROOT ]; then
    echo 'Downloading buildbox-worker...'
    git clone --depth 1 https://gitlab.com/BuildGrid/buildbox/buildbox-worker.git "$BUILDBOX_WORKER_SOURCE_ROOT"
fi
if [ ! -d $BUILDBOX_RUN_SOURCE_ROOT ]; then
    echo 'Downloading buildbox-run...'
    git clone --depth 1 https://gitlab.com/BuildGrid/buildbox/buildbox-run-hosttools.git "$BUILDBOX_RUN_SOURCE_ROOT"
fi
if [ ! -d $BUILDGRID_SOURCE_ROOT ]; then
    echo 'Downloading BuildGrid...'
    git clone --depth 1 https://gitlab.com/BuildGrid/buildgrid.git "$BUILDGRID_SOURCE_ROOT"
fi
if [ ! -d $RECC_SOURCE_ROOT ]; then
    echo 'Downloading RECC...'
    git clone --depth 1 https://gitlab.com/Bloomberg/recc.git "$RECC_SOURCE_ROOT"
fi

# Compile everything
CMAKE_OPTS="-DBUILD_TESTING=OFF -DCMAKE_INSTALL_PREFIX:PATH=$BUILDBOX_E2E_ROOT -DCMAKE_PREFIX_PATH=$BUILDBOX_E2E_ROOT"

echo 'Compiling buildbox-common'
(cd $BUILDBOX_COMMON_SOURCE_ROOT && mkdir -p build && cd build && cmake $CMAKE_OPTS .. && make install)
export BuildboxCommon_PATH=$BUILDBOX_E2E_ROOT

echo 'Compiling buildbox-worker'
(cd $BUILDBOX_WORKER_SOURCE_ROOT && mkdir -p build && cd build && cmake $CMAKE_OPTS .. && make)
BUILDBOX_WORKER_BINARY=$BUILDBOX_WORKER_SOURCE_ROOT/build/buildbox-worker

echo 'Compiling buildbox-run'
(cd $BUILDBOX_RUN_SOURCE_ROOT && mkdir -p build && cd build && cmake $CMAKE_OPTS .. && make)
BUILDBOX_RUN_BINARY=$BUILDBOX_RUN_SOURCE_ROOT/build/$BUILDBOX_RUN

echo 'Installing BuildGrid'
(cd $BUILDGRID_SOURCE_ROOT && pip3 install --upgrade --install-option="--prefix=$BUILDBOX_E2E_ROOT" .)
BUILDGRID_BINARY=$BUILDBOX_E2E_ROOT/bin/bgd
export PYTHONPATH=`echo $BUILDBOX_E2E_ROOT/lib/python3*/site-packages`

echo 'Compiling RECC'
(cd $RECC_SOURCE_ROOT && mkdir -p build && cd build && cmake $CMAKE_OPTS .. && make)
RECC_BINARY=$RECC_SOURCE_ROOT/build/bin/recc


$BUILDGRID_BINARY server start -v "$BUILDBOX_E2E_DATA_DIR/bgdconfig.yml" &
BUILDGRID_SERVER='localhost:50051'

sleep 1 # Give server some time to boot
netstat -pln | grep 50051 # Check for server existence
$BUILDBOX_WORKER_BINARY --verbose --buildbox-run=$BUILDBOX_RUN_BINARY --bots-remote=http://$BUILDGRID_SERVER --cas-remote=http://$BUILDGRID_SERVER &

export RECC_SERVER=$BUILDGRID_SERVER
export RECC_VERBOSE=1
export RECC_FORCE_REMOTE=1
export RECC_OUTPUT_FILES_OVERRIDE="hello"
export RECC_DEPS_OVERRIDE="hello.cpp"

BUILDBOX_E2E_TEMPORARY_DIRECTORY=`mktemp -d`
echo 'Running test in' $BUILDBOX_E2E_TEMPORARY_DIRECTORY
cp $BUILDBOX_E2E_DATA_DIR/hello.cpp $BUILDBOX_E2E_TEMPORARY_DIRECTORY
ln -s $RECC_BINARY $BUILDBOX_E2E_TEMPORARY_DIRECTORY/recc
(
    cd $BUILDBOX_E2E_TEMPORARY_DIRECTORY
    ./recc gcc hello.cpp -o hello;
    if [ "`./hello`" = "hello_world" ]; then echo "Correct output"; exit 0; else exit 1; fi
)
rm -rf $BUILDBOX_E2E_TEMPORARY_DIRECTORY
